# Example of the rmake command

[RU](README_RU.md)

[[_TOC_]]

Example operation of the [rmake](https://gitlab.com/neimus/rmake) command to enable a Makefile from the repository

## Configuring, installing and initializing rmake

1. Create a configuration file in the root of the project

```yaml
app:
  include_dir: "remote/Makefile"
  index_file: "index.mk"
  update_include_every: 1h
repositories:
  common-product:
    git_uri: "ssh://git@https://gitlab.com/neimus/rmake.git"
    branch_name: "master"
    files: [ "./example/hello-rmake.mk" ]
```

2. Start the installation of rmake and its initialization

```shell
go install gitlab.com/neimus/rmake@latest && rmake --init
```

Console output:

```shell
go: downloading gitlab.com/neimus/rmake v0.0.4
[INFO] The inclusion file in the Makefile has been successfully updated 
        {"group": common_product, "file_name": example/hello-rmake.mk}
[INFO] The inclusion file in the Makefile has been successfully updated 
        {"group": common_product, "file_name": example/hello-world.mk}
[INFO] The included Makefile files have been successfully updated
```

3. Connect the created file `remote/index.mk` to the Makefile

```
include remote/index.mk
...
```

Done!
Now you can run the included files

```shell
make hello-rmake
```

Console output:

```shell
echo "Hi! I included the file with the rmake command :)"
Hi! I included the file with the rmake command :)
```

Running `make` or `make all` will run `rmake`.
It will pull up dependency changes if they have been made (hash file) and the checkout time has arrived (
parameter `update_include_every`)

## Structure of folders and inclusion files

The structure created is as follows:

```
remote
    - rmake
        - hash
        - example
            - hello-rmake.mk
            - hello-world.mk
    - index.mk
```

- remote - folder with all included files
- remote/index.mk - index file with included files and rmake command
- remote/rmake - the group folder that contains all included files with the hash
- remote/rmake/hash - branch hash file. The time of creation/update of this file, serves as a reference for the
  following check (parameter `update_include_every`)

## PS

You can safely clone this repository, delete the `remote` folder and follow the instructions above
