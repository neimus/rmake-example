# Пример работы команды rmake

[EN](README.md)

[[_TOC_]]

Пример работы команды [rmake](https://gitlab.com/neimus/rmake) для включения Makefile из репозитория

## Настройка, установка и инициализация rmake

1. В корне проекта создадим конфигурационный файл

```yaml
app:
  include_dir: "remote/Makefile"
  index_file: "index.mk"
  update_include_every: 1h
repositories:
  common-product:
    git_uri: "ssh://git@https://gitlab.com/neimus/rmake.git"
    branch_name: "master"
    files: [ "./example/hello-rmake.mk" ]
```

2. Запустим установку rmake и ее инициализацию

```shell
go install gitlab.com/neimus/rmake@latest && rmake --init
```
Вывод консоли:
```shell
go: downloading gitlab.com/neimus/rmake v0.0.4
[INFO] The inclusion file in the Makefile has been successfully updated 
        {"group": common_product, "file_name": example/hello-rmake.mk}
[INFO] The inclusion file in the Makefile has been successfully updated 
        {"group": common_product, "file_name": example/hello-world.mk}
[INFO] The included Makefile files have been successfully updated
```

3. Подключим созданный файл `remote/index.mk` в Makefile

```
include remote/index.mk
...
```

Готово!
Теперь можно запустить включенные файлы
```shell
make hello-rmake
```

Вывод консоли:
```shell
echo "Hi! I included the file with the rmake command :)"
Hi! I included the file with the rmake command :)
```

При запуске `make` или `make all` отработает `rmake`. 
Он подтянет изменения зависимостей, если такие были сделаны (hash файл) и время проверки наступило (параметр `update_include_every`)


## Структура папок и файлов включений

Создана структура выглядит следующим образом:
```
remote
    - rmake
        - hash
        - example
            - hello-rmake.mk
            - hello-world.mk
    - index.mk
```
- remote - папка со всеми включенными файлами
- remote/index.mk - индексный файл с включенными файлами и запускаемой командой rmake
- remote/rmake - групповая папка, в которой содержаться все файлы включений с хешем
- remote/rmake/hash - хеш-файл ветки. Время создания/обновление этого файла, служит ориентиром для следующей проверки (параметр `update_include_every`)

## PS
Можно смело скачать данный репозиторий, удалив папку `remote` и пройтись по инструкции выше
