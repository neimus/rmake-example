.DEFAULT_GOAL := all

.PHONY: all
all:
	@rmake

include remote/rmake/example/hello-rmake.mk
include remote/rmake/example/hello-world.mk
